---
layout: post
title: How to import wordpress.com blog posts to jekyll blog for free
---

Wordpress.com is a nice place to host your blog if you want to write without worrying about technicalities. It offers a free plan in which you keep your blog under a subdomain of wordpress.com, but you need to upgrade your plan to use your own domain name. Plugins are not free either. Be aware that though wordpress.com is a paid platform, wordpress itself is a free tool maintained by it's open source community. You can host wordpress engine on a php server with mysql database support.

Wordpress has a plugin to export its posts to jekyll, named Jekyll Exporter authored by Ben Balter. But since plugins are a paid feature in wordpress.com, you cannot use that in a free plan. But remember, wordpress is a free tool and there is a workaround to using that plugin without paying. You can host a wordpress locally on your computer's php server and export your blog to it via an export file. From this local installation, you can use the jekyll exporter for free. The following walkthrough will help you with the steps to do so.

## Create an export file
From your wordpress admin panel, go to *Tools > Export*. From there, choose *All Content* and proceed to download. Save the downloaded export file to your desktop.

## Install wordpress locally
Search online on how to install wordpress locally on your operating system. Once its done proceed to next step.

## Import the export file
In your local wordpress installation admin panel, go to *Tools > Import* and import the export file that you saved earlier.

## Use the jekyll exporter plugin
Now that you have a local copy of your blog, go to plugins section and search for jekyll exporter, Install it and use the plugin to download a jekyll version of your blog as a zip file.

## Finally
Extract the zip file, you can either host its contents as is, or copy needed posts to an existing jekyll blog. In case you choose the later, you should also copy the directory named wp-contents, as it contains media files that you used in the blog.
