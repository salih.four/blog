---
title: Why I removed the original title of this Blog
date: 2019-06-06T14:36:53+00:00
author: Muhammed Salih
layout: post
timeline_notification:
  - "1559851617"
categories:
  - Thoughts
tags:
  - language
  - meaning
---
<figure id="attachment_52" aria-describedby="caption-attachment-52" style="width: 1280px" class="wp-caption aligncenter"><img class="alignnone size-full wp-image-52" src="/wp-content/uploads/2019/06/a-window-rarely-opened.jpg" alt="a-window-rarely-opened" width="400"/><figcaption id="caption-attachment-52" class="wp-caption-text">A window rarely opened</figcaption></figure> 

The original title of this blog page was H I G H C H A O S. I liked it all caps and with spaces in between all letters. I removed it. Here is why&#8230;

Yes, words have a decorative aspect to it. I like to see a good font and well balanced paragraphs on websites. I designed few websites myself. But above all, words are symbols, signs that hint at something else. When you pay attention to the word sheep, an image of that animal flashes in your mind. I started to admire this subtle factor. Words are what they mean, they resonate in our mind. A guy surrounded with good words get many positive thoughts in their day. If the same guy is surrounded with negative words, his thoughts take a different route. If that is not the case, then we would have to assume that the words do not ring a bell, like when the guy is not listening or is hearing a language unknown to him.

The ability of words to symbolise and resonate its meaning in reader&#8217;s mind is precious and should be handled with care. If we want to use a word frequently at unrelated places, we can. But then people eventually lose the meaning that word once resonated. Using words accurately and thereby preserving language is for our own good. I feel I should take time to illustrate this point with real examples in a future blog Inshallah.

Now here is why I changed the blog&#8217;s name. I do not want your day to seem more chaotic than usual and so I removed the name H I G H C H A O S. Rather you start looking at how connected and orderly the world around you is.