---
title: Thoughts on Consumer Culture
date: 2019-12-08T09:34:07+00:00
author: Muhammed Salih
layout: post
timeline_notification:
  - "1575817457"
categories:
  - Thoughts
tags:
  - consumerism
  - disposable
  - repairable
  - utility
  - waste
---
> **If it doesn&#8217;t add value, it&#8217;s waste** &#8211; Henry Ford

Name some product that is beloved to you, and think how long you have been using it. Here is my answer; a used laptop that I bought 18 months back &#8212; which right now could be over 5 years old &#8212; and I had never been dissatisfied with it. The laptop has a loose hinge which broke twice, and I do not complain, because I anticipated it would break when I bought it. The display is still intact and the keyboard and touch-pad has no issues. I had to install a memory module a few months back to keep up with my development needs. Summing up the costs, I have spend below Rs. 18000 till date.

Me buying this laptop has little cost on the environment, no resources were mined to make a laptop for me, instead I bought it from someone who no longer needed it. Is it not how business is supposed to be? Let me admit it, used products of our century have no market, it is risky to buy them as it can easily get damaged. It seems people have little attachment to things they own as they do not take care of them, and impart damage within a year or so. Companies are not concerned whether their products are repairable. Take mobiles for instance, manufacturers replace screws with glues to release slimmer versions, making it harder to repair. If a part gets damaged, buying another is preferred than getting it repaired.

Buying repairable and quality products lets you sell it when you are finished with it. Such furniture and utensils may live longer than their owners. On the other hand, buying and disposing materials has two effects, it drains earthly resources and nourish wastelands. If you ignore the adverts and buy only what you need, you will be happy. If you buy long living products, you can be content that they do not get disposed off like any other manufactured garbage.

<img class="alignnone size-full wp-image-68" src="http://localhost:8080/wp-content/uploads/2019/12/photo-1562077981-4d7eafd44932-e1575817322396.jpg" alt="photo-1562077981-4d7eafd44932.jpg" width="1620" height="764" />