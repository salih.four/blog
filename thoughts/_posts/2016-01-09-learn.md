---
title: Evolution of Individual
date: 2016-01-09T10:59:39+00:00
author: Muhammed Salih
layout: post
categories:
  - Thoughts
tags:
  - how to be
  - how to practice
  - learning
  - personality development
  - skill development
---
Know one way how a Guitarist or Shaolin Monk is different from you

#### ESSENCE

Our body and brain evolve according to the situations we put them into.  Consider a KG child who goes to drawing the letter &#8216;A&#8217; from plain memory. He would never rightly draw it at the first, second or third tries. Before he makes it, he has to keep the letter &#8216;A&#8217; in mind while tracing that letter on his workbook and has to consciously move his hand through dotted lines. This exercise will grow extra nerves and muscles at fingers over time like the muscle growth when people workout at the gym. After a while of practice, his sub-conscious mind aids him to draw it. By the time he reach higher grades, his brain is well tuned to his extra nerves grown at the fingers. The moment he thinks a letter, his brain repeats what he had been doing all those years. i.e.

> The brain signals hand to perform a set of moves which he once had to do with full focus of mind. And today his brain knows what set of signals to be send to hand when he thinks of a word or even word combinations.

#### EXAMPLES

This evolution goes with learning martial arts, playing musical instruments, drawing, peeling potatos, learning to walk, to write with left hand or to copy a carricature, and so on such that the parts involved in evolution differs. Relate the example of learning to write with any skill that can be earned by practice and figure out what to practice to learn it. It may increase thinking skill.

> Have a Great day