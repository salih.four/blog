---
title: Reflect on what you take in daily
date: 2019-12-07T11:02:53+00:00
author: Muhammed Salih
layout: post
timeline_notification:
  - "1575736376"
geo_public:
  - "0"
categories:
  - Thoughts
---
_I guess most readers would relate to this. Often I dive in to the internet, switch between social networks, and youtube to devour what they offer. I hate having wasted time on these content. I do not remember what they are now. They are not worth a place in my mind. Despite this regret, I still have the habit._

[<img class="aligncenter wp-image-63 " src="/wp-content/uploads/2019/12/matthew-guay-q7wddmgcbfg-unsplash-1.jpg?w=400&h=200&crop=1" alt="" width="440" height="220" />](http://localhost:8080/wp-content/uploads/2019/12/matthew-guay-q7wddmgcbfg-unsplash-1.jpg)

One effect of surfing over scattered information is that we grow distracted. Our attention is limited. We take breaks to regain focus. We sleep to enjoy another day. Now with that in mind, reflect on how much attention you pay to any content. Don&#8217;t you get excited at one and quickly swipe away to the next one? In a few days we form a habit of surfing over scattered content. It does not add value to us. It leave us craving for more distractions. We lose focus! Tell me, can you digest a content that you have paid little attention to?

We are unable to take decisions due to the scale of information we have. We cannot analyze them and draw conclusions. Deliberate or otherwise, there are misinformation around us, and they are bad. On the other hand, a truck load of facts are no good when they are scattered all over. Moreover, we naturally involve in entertaining content, and they are widely available. It is easy and stress free to get immersed in them, to escape into fantasy, to forget our existence and to forget God.