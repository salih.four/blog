---
title: May be you should have a look at privacy policies
date: 2015-11-13T12:24:50+00:00
author: Muhammed Salih
layout: post
categories:
  - Uncategorized
tags:
  - Google
  - Internet
  - Privacy
---
It has been a habit for ages, that people never really consider or even realize Privacy Policy Notices being there. Whatever might the sources be from where we have our dezired products, reading those policies are far more absurd than privacy leakage itself. Adding to this is the ideology that they are ill-designed so as no one prefer poking on it.

But one privacy policy changed Alfred&#8217;s attitude. Alfred is a Google fan.

As a non-professional, anyone would fancy Google. They have premium grade products which are both free and too awesome! Alfred began adding an extra &#8216;Google&#8217; term when he Googled for any app. He came over Google Keep, docs, slides, sheets, Googles wonderful Handwriting input for touchscreens, Google Now, and many other. Everywhere, he could notice himself agreeing the privacy policies and terms and conditions as if on Autopilot. Once he decided to have a look at it.

And it shocked him!

He figured that Google figured about him way more than his expectations. Google associates a lot of data with your account, they are collected through its diverse products, services and &#8216;partners&#8217; who use some sort of Google plugin on their web pages. He was heartily welcomed to be educated on how his own privacy was managed by Google.

Reading privacy policies of such Giant data collectors don&#8217;t seem to be that boring. Since the users agreement on them gives those companies the legal rights to have him spied, any unmentioned activities with privacy concerns would be taken seriously by people and the legislation. In simple words, they tend to really include detailed and true information on their data collection and conservation. Reading them are often wonderful and reflective! You may even be inspired to do what Google does.