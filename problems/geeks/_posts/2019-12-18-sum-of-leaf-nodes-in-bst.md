---
layout: post
title: Sum of leaf nodes in binary search tree
---
## Problem
[https://practice.geeksforgeeks.org/problems/sum-of-leaf-nodes-in-bst/1](https://practice.geeksforgeeks.org/problems/sum-of-leaf-nodes-in-bst/1)

## Solution

### Recursive
Method: Recursive <br>
Language: C++ <br>
Time complexity: O(n)

```cpp
int sumOfLeafNodes(Node *r ){
     /*Your code here */
     if(r==NULL) return 0;
     if(r->left == NULL and r->right == NULL) return r->data;
     else return sumOfLeafNodes(r->right) + sumOfLeafNodes(r->left);
}
```